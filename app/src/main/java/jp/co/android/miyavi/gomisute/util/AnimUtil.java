package jp.co.android.miyavi.gomisute.util;

import android.content.Context;
import android.os.Build;

/**
 * Created by m_iwasaki on 14/11/17.
 */
public class AnimUtil {

    /**
     * 早いアニメーション速度を取得(200ms).
     */
    public static int getShortAnimTime(Context context) {
        return context.getResources().getInteger(android.R.integer
                .config_shortAnimTime);
    }

    /**
     * 通常のアニメーション速度を取得(400ms).
     */
    public static int getMediumAnimTime(Context context) {
        return context.getResources().getInteger(android.R.integer
                .config_mediumAnimTime);
    }

    /**
     * 遅いアニメーション速度を取得(500ms).
     */
    public static int getLongAnimTime(Context context) {
        return context.getResources().getInteger(android.R.integer
                .config_longAnimTime);
    }

}
