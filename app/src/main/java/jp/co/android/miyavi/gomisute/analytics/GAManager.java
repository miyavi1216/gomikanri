package jp.co.android.miyavi.gomisute.analytics;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import android.content.Context;

import java.util.HashMap;

import jp.co.android.miyavi.gomisute.MyApplication;
import jp.co.miyavi.android.gomisute.R;

/**
 * GoogleAnalyticsのマネージャクラス.
 */
public class GAManager implements GAConsts {

    public enum TrackerName {
        APP_TRACKER
    }

    private HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    private MyApplication mApplication;

    public GAManager(MyApplication app) {
        this.mApplication = app;
    }

    /**
     * トラッカーを取得.
     */
    public synchronized Tracker getTracker(TrackerName trackerId) {
        if (!mTrackers.containsKey(trackerId)) {
            GoogleAnalytics ga = GoogleAnalytics.getInstance(mApplication);
            Tracker tracker = null;
            if (trackerId == TrackerName.APP_TRACKER) {
                tracker = ga.newTracker(R.xml.app_tracker);
            }
            tracker.enableAdvertisingIdCollection(true);
            tracker.enableExceptionReporting(true);
            mTrackers.put(trackerId, tracker);
        }
        return mTrackers.get(trackerId);
    }

    /**
     * スクリーンを送信.
     *
     * @param context コンテキスト
     * @param screen  スクリーン名
     */
    public static void sendScreen(Context context, String screen) {
        HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder();

        //送信
        Tracker tracker = MyApplication.getInstance().getTracker(TrackerName.APP_TRACKER);
        tracker.setScreenName(screen);
        tracker.send(builder.build());
    }

    /**
     * イベントを送信.
     */
    public static void sendEvent(Context context, String screen, String category, String event) {
        HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder(category, event);
        //送信
        Tracker tracker = MyApplication.getInstance().getTracker(TrackerName.APP_TRACKER);
        tracker.setScreenName(screen);
        tracker.send(builder.build());
    }
}