package jp.co.android.miyavi.gomisute.card;

import com.rey.material.app.DatePickerDialog;
import com.rey.material.app.Dialog;
import com.rey.material.app.DialogFragment;
import com.rey.material.app.TimePickerDialog;
import com.rey.material.widget.Button;
import com.rey.material.widget.Spinner;
import com.rey.material.widget.Switch;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import java.util.StringTokenizer;

import butterknife.Bind;
import butterknife.OnClick;
import butterknife.OnTouch;
import greendao.Gomi;
import jp.co.android.miyavi.gomisute.BaseActivity;
import jp.co.android.miyavi.gomisute.model.GomiData;
import jp.co.android.miyavi.gomisute.util.AnimUtil;
import jp.co.android.miyavi.gomisute.util.Funcs;
import jp.co.miyavi.android.gomisute.R;

public class CardCreateActivity extends BaseActivity {

    @Bind(R.id.textinput_name)
    TextInputLayout mTilInputName;

    @Bind(R.id.input_name)
    EditText mEtInputName;

    @Bind(R.id.input_display_name)
    EditText mEtInputDisplayName;

    @Bind(R.id.color_label)
    RadioGroup mRgColorLabel;

    @Bind(R.id.root_container)
    RelativeLayout mRlRootContainer;

    @Bind(R.id.pattern_spinner)
    Spinner mSpPattern;

    @Bind(R.id.remind_day_spinner)
    Spinner mSpRemindDay;

    @Bind(R.id.remind_container)
    LinearLayout mLlRemindContainer;

    @Bind(R.id.pattern_container)
    LinearLayout mLlPatternContainer;

    @Bind(R.id.dayofweek_container)
    LinearLayout mLlDayOfWeekContainer;

    @Bind(R.id.week_container)
    LinearLayout mLlWeekContainer;

    @Bind(R.id.pattern_date_select)
    Button mBtnDateSelect;

    @Bind(R.id.remind)
    Switch mSwRemind;

    @Bind(R.id.reminder_time_select)
    Button mBtnTimelect;

    public static interface Action {

        public static final String CREATE_CARD = "action_create_card";
    }

    private int[] mWeekCheckBoxIds = {R.id.week_1, R.id.week_2, R.id.week_3, R.id.week_4,
            R.id.week_5};

    private int[] mWeekOfDayCheckBoxIds = {R.id.dayofweek_mon, R.id.dayofweek_tue,
            R.id.dayofweek_wed, R.id.dayofweek_thu, R.id.dayofweek_fri, R.id.dayofweek_sat,
            R.id.dayofweek_sun};

    private int[] mColorLabelIds = {R.id.color_label_1, R.id.color_label_2, R.id.color_label_3,
            R.id.color_label_4, R.id.color_label_5, R.id.color_label_6, R.id.color_label_7};

    private Handler mHandler = new Handler();

    private GomiData mGomiData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //ToolBarの初期化
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setWhiteArrow(this);

        //ゴミデータ生成
        if (getIntent() != null) {
            mGomiData = getIntent().getParcelableExtra(IntentKey.GOMI_DATA);
        }
        if (mGomiData == null) {
            //データを新規作成
            mGomiData = new GomiData();
        }

        //基本情報設定
        setupBasicInfo();

        //収集日の設定
        setupPattern();

        //通知を設定
        setupRemind();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_create;
    }


    @Override
    protected void onDestroy() {
        //ゴミデータを保存
        saveData();

        super.onDestroy();
    }

    /**
     * 基本情報設定.
     */
    private void setupBasicInfo() {
        //タイトルをセット
        String name = mGomiData.getName();
        if (!TextUtils.isEmpty(name)) {
            setScreenTitle(getString(R.string.card_title_edit, name));
        } else {
            setScreenTitle(R.string.card_title_create);
        }

        //ゴミの種類をセット
        if (!TextUtils.isEmpty(name)) {
            mEtInputName.setText(name);
        } else {
            mTilInputName.setErrorEnabled(true);
            mTilInputName.setError(getString(R.string.card_input_error));
        }
        mEtInputName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (mEtInputName.getText().toString().length() == 0) {
                    mTilInputName.setErrorEnabled(true);
                    mTilInputName.setError(getString(R.string.card_input_error));
                } else {
                    mTilInputName.setErrorEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        //表示名をセット
        String displayName = mGomiData.getDisplayName();
        if (!TextUtils.isEmpty(displayName)) {
            mEtInputDisplayName.setText(displayName);
        }

        //カラーラベルをセット
        mRgColorLabel.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int colorRes = findColorByColorLabelId(radioGroup.getCheckedRadioButtonId());
                setScreenTitleBackgroundColor(colorRes);
                setScreenTitleColor(Color.WHITE);
                changeFocusView();
            }
        });
        int color = mGomiData.getColor();
        mRgColorLabel.check(mColorLabelIds[color - 1]);
    }

    /**
     * 収集日設定.
     */
    private void setupPattern() {
        //収集タイプのセット
        ArrayAdapter<CharSequence> patternAdapter = ArrayAdapter
                .createFromResource(this, R.array.patterns, R.layout.row_spn);
        patternAdapter.setDropDownViewResource(R.layout.row_spn_dropdown);
        mSpPattern.setAdapter(patternAdapter);
        mSpPattern.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(Spinner spinner, View view, int i, long l) {
                showPatternContents(i, true);
            }
        });
        int collectionType = mGomiData.getCollectionType();

        //収集タイプ別にデータをセット
        if (collectionType == GomiData.CollectionType.EVERY_WEEK) {
            //毎週
            createWeekCheckBox(mGomiData.getCollectionWeek());
        } else if (collectionType == GomiData.CollectionType.SELECT_WEEK) {
            //週指定
            createWeekCheckBox(mGomiData.getCollectionWeek());
            createWeekOfDayCheckBox(mGomiData.getCollectionWeekOfDay());
        } else if (collectionType == GomiData.CollectionType.SELECT_DATE) {
            //日付指定
            String date = Funcs.convertTimeToLong(mGomiData.getCollectionDate());
            mBtnDateSelect.setText(date);
        }

        //収集タイプを表示
        mSpPattern.setSelection(collectionType);
        showPatternContents(collectionType, false);
    }

    /**
     * 週のチェックボックスを作成.
     */
    private void createWeekCheckBox(String value) {
        if (!TextUtils.isEmpty(value)) {
            StringTokenizer st = new StringTokenizer(value, ",");
            while (st.hasMoreTokens()) {
                int index = Integer.valueOf(st.nextToken()) - 1;
                final CheckBox cbWeek = (CheckBox) findViewById(mWeekCheckBoxIds[index]);
                cbWeek.setChecked(true);
            }
        }
    }

    /**
     * 曜日のチェックボックスを作成.
     */
    private void createWeekOfDayCheckBox(String value) {
        if (!TextUtils.isEmpty(value)) {
            StringTokenizer st = new StringTokenizer(value, ",");
            while (st.hasMoreTokens()) {
                int index = Integer.valueOf(st.nextToken()) - 1;
                final CheckBox cbWeek = (CheckBox) findViewById(mWeekOfDayCheckBoxIds[index]);
                cbWeek.setChecked(true);
            }
        }
    }

    /**
     * 通知設定.
     */
    private void setupRemind() {
        //通知日をセット
        ArrayAdapter<CharSequence> remindAdapter = ArrayAdapter
                .createFromResource(this, R.array.remind_days, R.layout.row_spn);
        remindAdapter.setDropDownViewResource(R.layout.row_spn_dropdown);
        mSpRemindDay.setAdapter(remindAdapter);
        mSwRemind.setOnCheckedChangeListener(new Switch.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(Switch aSwitch, boolean b) {
                showRemindContents(b);
            }
        });
        boolean remind = mGomiData.getRemind() == 1 ? true : false;
        mSwRemind.setChecked(remind);

        if (remind) {
            //通知タイプをセット
            int remindType = mGomiData.getRemindType();
            mSpRemindDay.setSelection(remindType);

            //通知日時をセット
            int hour = mGomiData.getRemindHour();
            int minutes = mGomiData.getRemindMinutes();
            mBtnTimelect
                    .setText(String.format("%02d", hour) + ":" + String.format("%02d", minutes));
        }
    }

    /**
     * 編集データを保存.
     */
    private void saveData() {
        String name = mEtInputName.getText().toString();
        if (!TextUtils.isEmpty(name)) {
            //名前を保存
            mGomiData.setName(name);
        } else {
            //名前がなかったら保存させない
            return;
        }

        //表示名を保存
        String displayName = mEtInputDisplayName.getText().toString();
        if (!TextUtils.isEmpty(displayName)) {
            mGomiData.setDisplayName("資源");
        }

        //カラーを保存
        int radioId = mRgColorLabel.getCheckedRadioButtonId();
        int color = GomiData.Color.BLUE_GREY;
        for (int i = 0; i < mColorLabelIds.length; i++) {
            if (radioId == mColorLabelIds[i]) {
                color = i + 1;
            }
        }
        mGomiData.setColor(color);

        //収集タイプを保存
        int collectionType = mSpPattern.getSelectedItemPosition();
        mGomiData.setCollectionType(collectionType);

        //収集日の曜日を保存
        if (collectionType == GomiData.CollectionType.EVERY_WEEK
                || collectionType == GomiData.CollectionType.SELECT_WEEK) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mWeekOfDayCheckBoxIds.length; i++) {
                CheckBox cb = (CheckBox) findViewById(mWeekOfDayCheckBoxIds[i]);
                String weekOfDay = null;
                if (cb.isChecked()) {
                    weekOfDay = String.valueOf(i + 1);
                }
                if (!TextUtils.isEmpty(weekOfDay)) {
                    sb.append(weekOfDay).append(",");
                }
            }
            String collectionWeekOfDay = sb.toString();
            if (!TextUtils.isEmpty(collectionWeekOfDay)) {
                mGomiData.setCollectionWeekOfDay(
                        collectionWeekOfDay.substring(0, collectionWeekOfDay.length() - 1));
            } else {
                return;
            }
        }

        //収集日の週を保存
        if (collectionType == GomiData.CollectionType.SELECT_WEEK) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mWeekCheckBoxIds.length; i++) {
                CheckBox cb = (CheckBox) findViewById(mWeekCheckBoxIds[i]);
                String week = null;
                if (cb.isChecked()) {
                    week = String.valueOf(i + 1);
                }
                if (!TextUtils.isEmpty(week)) {
                    sb.append(week).append(",");
                }
            }
            String collectionWeek = sb.toString();
            if (!TextUtils.isEmpty(collectionWeek)) {
                mGomiData.setCollectionWeek(
                        collectionWeek.substring(0, collectionWeek.length() - 1));
            } else {
                return;
            }
        }

        //収集タイプが日付指定じゃなかった場合は念のため収集日付をリセット
        if (collectionType != GomiData.CollectionType.SELECT_DATE) {
            mGomiData.setCollectionDate(0);
        }

        //通知設定を保存
        int remind = mSwRemind.isChecked() == true ? 1 : 0;
        mGomiData.setRemind(remind);

        //通知タイプを保存
        int remindType = mSpRemindDay.getSelectedItemPosition();
        mGomiData.setRemindType(remindType);

        //通知OFFで保存する場合は念のため通知時間をリセット
        if (remind == GomiData.Remind.OFF) {
            mGomiData.setRemindHour(0);
            mGomiData.setRemindMinutes(0);
        }

        //作成時間を保存
        mGomiData.setCreatedTime(System.currentTimeMillis());

        createCard(mGomiData);

    }

    /**
     * ゴミカードを作成.
     */
    private void createCard(GomiData data) {
        Intent resultData = new Intent(Action.CREATE_CARD);
        resultData.putExtra(IntentKey.GOMI_DATA, data);
        LocalBroadcastManager.getInstance(this).sendBroadcast(resultData);
    }

    /**
     * 収集日のコンテンツ表示アニメーション.
     */
    private void showPatternContents(final int pattern, final boolean isAnimation) {
        AlphaAnimation alphaOut = new AlphaAnimation(1F, 0F);
        if (isAnimation) {
            alphaOut.setDuration(AnimUtil.getShortAnimTime(this));
        } else {
//            alphaOut.setDuration(0);
        }
        alphaOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (pattern == GomiData.CollectionType.EVERY_WEEK) {
                    mLlWeekContainer.setVisibility(View.GONE);
                    mLlDayOfWeekContainer.setVisibility(View.VISIBLE);
                    mBtnDateSelect.setVisibility(View.GONE);
                } else if (pattern == GomiData.CollectionType.SELECT_WEEK) {
                    mLlWeekContainer.setVisibility(View.VISIBLE);
                    mLlDayOfWeekContainer.setVisibility(View.VISIBLE);
                    mBtnDateSelect.setVisibility(View.GONE);
                } else if (pattern == GomiData.CollectionType.SELECT_DATE) {
                    mLlWeekContainer.setVisibility(View.GONE);
                    mLlDayOfWeekContainer.setVisibility(View.GONE);
                    mBtnDateSelect.setVisibility(View.VISIBLE);
                }

                AlphaAnimation alphaIn = new AlphaAnimation(0F, 1F);
                alphaIn.setDuration(AnimUtil.getShortAnimTime(CardCreateActivity.this));
                mLlPatternContainer.setAnimation(alphaIn);
                mLlPatternContainer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        mLlPatternContainer.setAnimation(alphaOut);
        mLlPatternContainer.setVisibility(View.INVISIBLE);
    }

    /**
     * 通知のコンテンツ表示アニメーション.
     */
    private void showRemindContents(boolean state) {
        AlphaAnimation alpha;
        if (state) {
            alpha = new AlphaAnimation(0F, 1F);
            alpha.setDuration(AnimUtil.getMediumAnimTime(CardCreateActivity.this));
            mLlRemindContainer.setAnimation(alpha);
            mLlRemindContainer.setVisibility(View.VISIBLE);
        } else {
            alpha = new AlphaAnimation(1F, 0F);
            alpha.setDuration(AnimUtil.getMediumAnimTime(CardCreateActivity.this));
            mLlRemindContainer.setAnimation(alpha);
            mLlRemindContainer.setVisibility(View.GONE);
        }
    }

    @OnTouch(R.id.root_container)
    boolean onTouch() {
        changeFocusView();
        return false;
    }

    @OnClick(R.id.pattern_date_select)
    void onClickDateSelect() {
        Dialog.Builder builder = new DatePickerDialog.Builder(R.style.DatePickerDialogStyle) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                DatePickerDialog dialog = (DatePickerDialog) fragment.getDialog();
                long date = dialog.getDate();
                mGomiData.setCollectionDate(date);
                String value = Funcs.convertTimeToLong(date);
                mBtnDateSelect.setText(value);
                super.onPositiveActionClicked(fragment);
            }
        };
        builder.positiveAction(getString(android.R.string.ok))
                .negativeAction(getString(android.R.string.cancel));
        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(), null);
    }

    @OnClick(R.id.reminder_time_select)
    void onClickTimeSelect() {
        Dialog.Builder builder = new TimePickerDialog.Builder(R.style.TimePickerDialogStyle,
                6, 00) {
            @Override
            public void onPositiveActionClicked(DialogFragment fragment) {
                TimePickerDialog dialog = (TimePickerDialog) fragment.getDialog();
                int hour = dialog.getHour();
                int minutes = dialog.getMinute();
                mGomiData.setRemindHour(hour);
                mGomiData.setRemindMinutes(minutes);
                mBtnTimelect.setText(hour + ":" + minutes);
                super.onPositiveActionClicked(fragment);
            }
        };
        builder.positiveAction(getString(android.R.string.ok))
                .negativeAction(getString(android.R.string.cancel));
        DialogFragment fragment = DialogFragment.newInstance(builder);
        fragment.show(getSupportFragmentManager(), null);
    }

    /**
     * EditTextからフォーカスを外す.
     */
    private void changeFocusView() {
        mRlRootContainer.requestFocus();
        Funcs.closeIme(this.getCurrentFocus());
    }

    /**
     * カラーラベルのIDからカラーリソースを探す.
     */
    private int findColorByColorLabelId(int id) {
        int color = getResources().getColor(R.color.md_blue_grey_300);
        switch (id) {
            case R.id.color_label_1:
                color = getResources().getColor(R.color.md_blue_grey_300);
                break;
            case R.id.color_label_2:
                color = getResources().getColor(R.color.md_red_300);
                break;
            case R.id.color_label_3:
                color = getResources().getColor(R.color.md_deep_orange_300);
                break;
            case R.id.color_label_4:
                color = getResources().getColor(R.color.md_indigo_300);
                break;
            case R.id.color_label_5:
                color = getResources().getColor(R.color.md_deep_purple_300);
                break;
            case R.id.color_label_6:
                color = getResources().getColor(R.color.md_green_300);
                break;
            case R.id.color_label_7:
                color = getResources().getColor(R.color.md_teal_300);
                break;
        }
        return color;
    }
}