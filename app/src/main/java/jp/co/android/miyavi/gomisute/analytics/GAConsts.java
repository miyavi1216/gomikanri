package jp.co.android.miyavi.gomisute.analytics;

/**
 * GoogleAnalyticsで利用するコンスト.
 */
public interface GAConsts {

    /**
     * スクリーン
     */
    public interface Screens {

        public static final String CARD_LIST = "CARD_LIST";
        public static final String CARD_CREATE = "CARD_CREATE";
        public static final String CALENDAR = "CALENDAR";
        public static final String RURL = "RURL";
        public static final String SETTING = "SETTING";
    }

    /**
     * カテゴリ
     */
    public interface Categories {
        public static final String CARD_MODE = "CARD_MODE";
        public static final String CARD_CONTROL = "CARD_CONTROL";
        public static final String COMMON_CONTROL = "COMMON_CONTROL";
    }

    /**
     * アクション
     */
    public interface Actions {
        public static final String TOTAL_CARD_NUMBER = "TOTAL_CARD_NUMBER";
        public static final String REMOVE_CARD = "REMOVE_CARD";
        public static final String EDIT_CARD = "EDIT_CARD";
        public static final String CREATE_CARD = "CREATE_CARD";
        public static final String MOVE_CARD = "MOVE_CARD";
        public static final String CLICK_PAID = "CLICK_PAID";
    }
}