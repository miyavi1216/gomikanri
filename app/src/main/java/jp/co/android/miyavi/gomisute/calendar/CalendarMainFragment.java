package jp.co.android.miyavi.gomisute.calendar;

import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.Bind;
import jp.co.android.miyavi.gomisute.BaseFragment;
import jp.co.miyavi.android.gomisute.R;

public class CalendarMainFragment extends BaseFragment {

    /** 定数 */
    public static final String TAG = CalendarMainFragment.class.getSimpleName();

    /** フィールド */
    @Bind(R.id.calendar)
    MaterialCalendarView calender;

    @Bind(R.id.background)
    View mBackground;

    public static CalendarMainFragment newInstance() {
        return new CalendarMainFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);

        calender.setTitleFormatter(new JpDateFormatTitleFormatter());
        return root;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_calendar_main;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setBackgroundAlpha(float alpha) {
        if (mBackground != null) {
            mBackground.setAlpha(alpha);
        }
    }
}