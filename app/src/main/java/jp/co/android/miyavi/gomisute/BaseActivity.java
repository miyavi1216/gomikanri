package jp.co.android.miyavi.gomisute;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import jp.co.android.miyavi.gomisute.view.NotoJpTextView;
import jp.co.miyavi.android.gomisute.R;

public abstract class BaseActivity extends AppCompatActivity {
    public static interface IntentKey {

        public static final String GOMI_DATA = "intent_key_gomi_data";
    }

    @Bind(R.id.toolbar)
    protected Toolbar mToolBar;

    @Bind(R.id.title)
    protected NotoJpTextView mTvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(getLayoutResource());

        ButterKnife.bind(this);

        //ToolBarの初期化
        mTvTitle.setTypeface(Typeface.DEFAULT, Typeface.NORMAL);
        setSupportActionBar(mToolBar);
    }

    protected abstract int getLayoutResource();

    /**
     * タイトルをセット.
     */
    public void setScreenTitle(int titleRes) {
        mTvTitle.setText(getString(titleRes));
    }

    /**
     * タイトルをセット.
     */
    public void setScreenTitle(String title) {
        mTvTitle.setText(title);
    }

    /**
     * 矢印を白くする.
     */
    public void setWhiteArrow(Context context) {
        final Drawable upArrow = getResources()
                .getDrawable(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
    }

    /**
     * タイトルカラーをセット.
     */
    public void setScreenTitleColor(int color) {
        mTvTitle.setTextColor(color);
    }

    /**
     * タイトルの背景カラーをセット.
     */
    public void setScreenTitleBackgroundColor(int color) {
        mToolBar.setBackgroundColor(color);
    }

    /**
     * トーストを表示.
     */
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
