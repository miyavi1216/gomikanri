package jp.co.android.miyavi.gomisute.util;

import android.app.Application;
import android.util.Log;

import java.util.regex.Pattern;

/**
 * Log出力ユーティリティ.
 * 
 * @author Re:Kayo-System / m.iwasaki
 */
public class LogUtil extends Application {
    public static final boolean DEBUG = true;

    /**
     * スタックトレースを取得.
     * 
     * @return
     */
    public static final String getTag() {
        final StackTraceElement trace = Thread.currentThread().getStackTrace()[4];
        final String cla = trace.getClassName();
        Pattern pattern = Pattern.compile("[\\.]+");
        final String[] splitedStr = pattern.split(cla);
        final String simpleClass = splitedStr[splitedStr.length - 1];
        final String mthd = trace.getMethodName();
        final int line = trace.getLineNumber();
        final String tag = simpleClass + "#" + mthd + ":" + line;
        return tag;
    }

    /**
     * メソッド開始.
     * 
     * @param tag
     */
    public static void enter() {
        if (DEBUG) {
            Log.d(getTag(), "enter Method");
        }
    }

    /**
     * メソッド終了.
     * 
     * @param tag
     */
    public static void exit() {
        if (DEBUG) {
            Log.d(getTag(), "exit Method");
        }
    }

    /**
     * デバッグログ出力.
     * 
     * @param msg
     */
    public static void d(String msg) {
        if (DEBUG) {
            Log.d(getTag(), msg);
        }
    }

    /**
     * エラーログ出力.
     * 
     * @param msg
     */
    public static void e(String msg) {
        if (DEBUG) {
            Log.e(getTag(), msg);
        }
    }
}
