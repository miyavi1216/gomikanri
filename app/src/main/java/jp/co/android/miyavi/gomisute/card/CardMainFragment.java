package jp.co.android.miyavi.gomisute.card;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.Bind;
import greendao.Gomi;
import greendao.GomiDao;
import jp.co.android.miyavi.gomisute.BaseFragment;
import jp.co.android.miyavi.gomisute.MyApplication;
import jp.co.android.miyavi.gomisute.dao.GomiDaoHelper;
import jp.co.android.miyavi.gomisute.model.GomiData;
import jp.co.android.miyavi.gomisute.view.EmptyRecyclerView;
import jp.co.android.miyavi.gomisute.view.NotoJpTextView;
import jp.co.miyavi.android.gomisute.R;
import jp.wasabeef.recyclerview.animators.LandingAnimator;

public class CardMainFragment extends BaseFragment
        implements CardRecycleAdapter.OnActionItem, LoaderManager.LoaderCallbacks<Cursor> {

    /** 定数 */
    public static final String TAG = CardMainFragment.class.getSimpleName();

    public static final String KEY_URL = "key-url";

    /** フィールド */
    @Bind(R.id.recyclerview)
    EmptyRecyclerView mRecyclerView;

    private Cursor mCursor;

    private GomiCursorLoader mCursorLoader;

    public static CardMainFragment newInstance() {
        return new CardMainFragment();
    }


    private CardRecycleAdapter mAdapter;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);

        setupRecyclerView(root);

        //カード作成レシーバを登録
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                mAddCardReceiver, new IntentFilter(CardCreateActivity.Action.CREATE_CARD));

        return root;
    }

    private void setupRecyclerView(View root) {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerView.setItemAnimator(new LandingAnimator());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setEmptyView(setupEmptyView(root));

        mAdapter = new CardRecycleAdapter(mActivity, mCursor);
        mAdapter.setOnActionItemListener(this);
        mRecyclerView.setAdapter(mAdapter);
    }

    /**
     * EmptyViewの初期化.
     */
    private View setupEmptyView(View root) {
        View emptyView = root.findViewById(R.id.emptyview);
        NotoJpTextView textView = (NotoJpTextView) emptyView.findViewById(R.id.description);
        textView.setTypeface(Typeface.DEFAULT, Typeface.BOLD_ITALIC);
        return emptyView;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_card_main;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(R.layout.fragment_card_main, null, this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        //カード作成レシーバを解除
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mAddCardReceiver);

        //RecyclerViewを開放
        mRecyclerView.removeAllViews();

        super.onDestroyView();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return createGomiCursorLoader();
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mCursor = data;
        mAdapter.swapCursor(mCursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        if (mAdapter != null) {
            mAdapter.swapCursor(null);
        }
    }

    /**
     * 読み込むカーソルローダーを作成.
     */
    protected CursorLoader createGomiCursorLoader() {
        mCursorLoader = new GomiCursorLoader(mActivity);
        return mCursorLoader;
    }

    /**
     * カーソルを更新.
     */
    protected void refreshCursor() {
        mCursor = getGomiCursor();
        if (mCursor != null) {
            mAdapter.swapCursor(mCursor);
            mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * ゴミリストのCursorを返却.
     */
    protected Cursor getGomiCursor() {
        return mCursorLoader.loadInBackground();
    }


    /**
     * クーポン用のカーソルローダー.
     */
    public static class GomiCursorLoader extends CursorLoader {

        public GomiCursorLoader(Context context) {
            super(context);
        }

        @Override
        public Cursor loadInBackground() {
            GomiDao gomiDao = MyApplication.getInstance().getDaoSession().getGomiDao();
            StringBuilder orderBy = new StringBuilder();
            orderBy.append(GomiDao.Properties.CreatedTime.columnName)
                    .append(" ").append("DESC");
            return gomiDao.getDatabase().query(gomiDao.getTablename(),
                    gomiDao.getAllColumns(), null, null, null, null, orderBy.toString());
        }

    }

    @Override
    public void onEditItem(View view, Gomi data) {
        //カード編集画面へ移動
        ActivityOptionsCompat options = ActivityOptionsCompat
                .makeSceneTransitionAnimation(mActivity, view, getString(R.string.se_view));
        Intent intent = new Intent(mActivity, CardCreateActivity.class);
        intent.putExtra(CardCreateActivity.IntentKey.GOMI_DATA, new GomiData(data));
        ActivityCompat.startActivity(mActivity, intent, options.toBundle());
    }

    /**
     * カード作成・変更を検知するBroadcastReceiver.
     */
    BroadcastReceiver mAddCardReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //カードを追加
            GomiData data = intent.getParcelableExtra(CardCreateActivity.IntentKey.GOMI_DATA);
            if (data != null) {
                GomiDaoHelper.save(GomiData.parseGomiData(data));
                refreshCursor();
            }
        }
    };

    public int getGomiCount() {
        if (mCursor == null) {
            return 0;
        }
        return mCursor.getCount();
    }
}