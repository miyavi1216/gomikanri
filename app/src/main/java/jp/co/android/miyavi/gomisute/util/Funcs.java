package jp.co.android.miyavi.gomisute.util;

import android.content.Context;
import android.os.Build;
import android.os.IBinder;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.text.SimpleDateFormat;
import java.util.Date;

import jp.co.android.miyavi.gomisute.model.GomiData;
import jp.co.miyavi.android.gomisute.R;

/**
 * Created by m_iwasaki on 14/11/17.
 */
public class Funcs {

    /**
     * Lillipopかどうか確認.
     */
    public static boolean isLollipop() {
        boolean isLollipop = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            isLollipop = true;
        }
        return isLollipop;
    }

    /**
     * キーボードを非表示にする.
     */
    public static void closeIme(View view) {
        if (view == null) {
            return;
        }
        IBinder windowToken = view.getWindowToken();
        if (windowToken != null) {
            Context context = view.getContext();
            InputMethodManager inputMethodManager = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(windowToken,
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /**
     * long値から時間を生成.
     */
    public static String convertTimeToLong(Long value) {
        Date date = new Date(value);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
        return sdf.format(date);
    }

    /**
     * ゴミ出しルールURLを取得.
     */
    public static int getColorResource(Context context, int colorlabel) {
        int colorRes = context.getResources().getColor(R.color.md_blue_grey_300);
        switch (colorlabel) {
            case GomiData.Color.BLUE_GREY:
                colorRes = context.getResources().getColor(R.color.md_blue_grey_300);
                break;
            case GomiData.Color.RED:
                colorRes = context.getResources().getColor(R.color.md_red_300);
                break;
            case GomiData.Color.DEEP_ORANGE:
                colorRes = context.getResources().getColor(R.color.md_deep_orange_300);
                break;
            case GomiData.Color.INDIGO:
                colorRes = context.getResources().getColor(R.color.md_indigo_300);
                break;
            case GomiData.Color.DEEP_PURPLE:
                colorRes = context.getResources().getColor(R.color.md_deep_purple_300);
                break;
            case GomiData.Color.GREEN:
                colorRes = context.getResources().getColor(R.color.md_green_300);
                break;
            case GomiData.Color.TEAL:
                colorRes = context.getResources().getColor(R.color.md_teal_300);
                break;
        }
        return colorRes;
    }
}
