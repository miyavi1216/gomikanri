
package jp.co.android.miyavi.gomisute.typeface;

import android.content.Context;
import android.graphics.Typeface;

import java.util.Hashtable;

import jp.co.android.miyavi.gomisute.util.LogUtil;

public class NotoJpTypefaces {

    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

    private static Typeface mInstance = null;

    public static Typeface getInstance(Context context) {
        if (mInstance == null) {
            mInstance = get(context, "fonts/NotoSansCJKjp-Light.otf");
        }
        return mInstance;
    }

    public static Typeface get(Context c, String assetPath) {
        synchronized (cache) {
            if (!cache.containsKey(assetPath)) {
                try {
                    Typeface t = Typeface.createFromAsset(c.getAssets(), assetPath);
                    cache.put(assetPath, t);
                } catch (Exception e) {
                    LogUtil.e("Could not get typeface '" + assetPath + "' because "
                            + e.getMessage());
                    return null;
                }
            }
            return cache.get(assetPath);
        }
    }
}
