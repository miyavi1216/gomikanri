package jp.co.android.miyavi.gomisute.pref;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * PrefHelper.
 */
public class PrefHelper {

    public static interface Key {
        public static final String KEY_LOCAL_RULE_URL = "key_local_rule_url";
        public static final String KEY_GORGEOUS = "key_gorgeous";
    }

    /**
     * ゴミ出しルールURLを取得.
     */
    public static String getRuleUrl(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getString(Key.KEY_LOCAL_RULE_URL, null);
    }

}
