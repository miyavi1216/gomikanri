package jp.co.android.miyavi.gomisute;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.OnClick;
import jp.co.android.miyavi.gomisute.calendar.CalendarMainFragment;
import jp.co.android.miyavi.gomisute.card.CardCreateActivity;
import jp.co.android.miyavi.gomisute.card.CardMainFragment;
import jp.co.android.miyavi.gomisute.model.GomiData;
import jp.co.android.miyavi.gomisute.pref.PrefHelper;
import jp.co.android.miyavi.gomisute.rule.RulePageActivity;
import jp.co.android.miyavi.gomisute.setting.SettingActivity;
import jp.co.android.miyavi.gomisute.util.AnimUtil;
import jp.co.android.miyavi.gomisute.util.Funcs;
import jp.co.android.miyavi.gomisute.view.MainPagerAdapter;
import jp.co.android.miyavi.gomisute.view.ViewPagerContainer;
import jp.co.miyavi.android.gomisute.R;


public class MainActivity extends BaseActivity {

    public static final int RESULT_SETTING = 0x9901;

    @Bind(R.id.viewpager)
    ViewPager mViewPager;

    @Bind(R.id.create_card)
    FloatingActionButton mCreateCardButton;

    @Bind(R.id.tabs)
    TabLayout mTabLayout;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //タイトルを設定
        setScreenTitle(R.string.app_name);

        //CreateButtonの余白を調整
        setMargintoCreateButton();

        // メインコンテンツのセットアップ
        setupContents();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_SETTING) {
            //オプションメニューを更新(ゴミ出しルール追加用)
            invalidateOptionsMenu();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        //ルールボタンの表示設定
        String url = PrefHelper.getRuleUrl(this);
        if (TextUtils.isEmpty(url)) {
            //URLがなければ非表示
            menu.findItem(R.id.action_rule).setVisible(false);
        } else {
            //URLがあれば表示
            menu.findItem(R.id.action_rule).setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_rule) {
            String url = PrefHelper.getRuleUrl(this);
            Intent intent = new Intent(this, RulePageActivity.class);
            intent.putExtra(RulePageActivity.INTENT_KEY_URL, url);
            startActivity(intent);

        } else if (id == R.id.action_settings) {
            startActivityForResult(new Intent(this, SettingActivity.class), RESULT_SETTING);
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.create_card)
    void onClickCreateCard(View view) {
        //カード作成画面を呼び出す
        ActivityOptionsCompat options = ActivityOptionsCompat
                .makeSceneTransitionAnimation(this, view, getString(R.string.se_view));
        Intent intent = new Intent(this, CardCreateActivity.class);
        ActivityCompat.startActivity(this, intent, options.toBundle());
    }


    /**
     * FloatingActionButtonにMarginをセット(バグでLollipopとそれ以外でMarginの取られ方が変わる).
     */
    private void setMargintoCreateButton() {
        CoordinatorLayout.LayoutParams params
                = (CoordinatorLayout.LayoutParams) mCreateCardButton.getLayoutParams();
        int margin = 0;
        if (Funcs.isLollipop()) {
            margin = getResources().getDimensionPixelSize(R.dimen.padding_large);
        } else {
            margin = getResources().getDimensionPixelSize(R.dimen.padding_small);
        }
        params.setMargins(margin, margin, margin, margin);
        mCreateCardButton.setLayoutParams(params);
    }

    /**
     * メインコンテンツをセットアップ.
     */
    private void setupContents() {
        //コンテンツの作製
        final ArrayList<ViewPagerContainer> contents = new ArrayList<>();
        contents.add(new ViewPagerContainer(CardMainFragment.newInstance(),
                getString(R.string.page_gomi_list)));
        contents.add(new ViewPagerContainer(CalendarMainFragment.newInstance(),
                getString(R.string.page_gomi_calendar)));
        MainPagerAdapter adapter = new MainPagerAdapter(getSupportFragmentManager(), contents);

        //ViewPagerの初期化
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                    int positionOffsetPixels) {
                if (positionOffset > 0) {
                    changeCalendarAlpha(positionOffset);
                }
            }

            @Override
            public void onPageSelected(int position) {

                if (position == 0) {
                    //カードページではFABを表示
                    showFloatingActionButton(true);
                    changeCalendarAlpha(0);
                } else {
                    //カレンダーページではFABを非表示
                    showFloatingActionButton(false);
                    changeCalendarAlpha(1);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

            private void changeCalendarAlpha(float value) {
                CardMainFragment cardMain = (CardMainFragment) contents.get(0).getFragment();
                CalendarMainFragment calendarMain = (CalendarMainFragment) contents.get(1)
                        .getFragment();
                if (cardMain != null && calendarMain != null) {
                    if (cardMain.getGomiCount() > 0) {
                        calendarMain.setBackgroundAlpha(value);
                    }
                }
            }
        });
        //TabLayoutの初期化
        mTabLayout.setTabTextColors(getResources().getColor(R.color.md_blue_grey_500),
                getResources().getColor(R.color.color_accent));
        mTabLayout.setupWithViewPager(mViewPager);
    }

    private void showFloatingActionButton(boolean visible) {
        mCreateCardButton.clearAnimation();
        AnimatorSet scaling = new AnimatorSet();
        ObjectAnimator scaleX = null;
        ObjectAnimator scaleY = null;
        if (visible) {
            scaleX = ObjectAnimator.ofFloat(mCreateCardButton, "scaleX", 1.0f);
            scaleY = ObjectAnimator.ofFloat(mCreateCardButton, "scaleY", 1.0f);
            scaling.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    if (mCreateCardButton != null) {
                        mCreateCardButton.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }
            });
        } else {
            scaleX = ObjectAnimator.ofFloat(mCreateCardButton, "scaleX", 0.0f);
            scaleY = ObjectAnimator.ofFloat(mCreateCardButton, "scaleY", 0.0f);
            scaling.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (mCreateCardButton != null) {
                        mCreateCardButton.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }
            });
        }

        //アニメーションの設定
        int speed = AnimUtil.getShortAnimTime(this);
        scaleX.setDuration(speed);
        scaleY.setDuration(speed);
        scaling.play(scaleX).with(scaleY);
        scaling.start();
    }
}
