package jp.co.android.miyavi.gomisute.card;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupMenu;

import java.util.StringTokenizer;

import butterknife.Bind;
import butterknife.ButterKnife;
import greendao.Gomi;
import greendao.GomiDao;
import jp.co.android.miyavi.gomisute.dao.GomiDaoHelper;
import jp.co.android.miyavi.gomisute.model.GomiData;
import jp.co.android.miyavi.gomisute.view.CursorRecyclerViewAdapter;
import jp.co.android.miyavi.gomisute.view.NotoJpTextView;
import jp.co.miyavi.android.gomisute.R;

public class CardRecycleAdapter extends CursorRecyclerViewAdapter<CardRecycleAdapter.ViewHolder> {

    private LayoutInflater mLayoutInflater;

    private Context mContext;

    private OnActionItem mListener;

    public CardRecycleAdapter(Context context, Cursor cursor) {
        super(context, cursor);
        mLayoutInflater = LayoutInflater.from(context);
        mContext = context;
    }

    @Override
    public CardRecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = mLayoutInflater.inflate(R.layout.item_card, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void bindView(final ViewHolder holder, Context context, Cursor cursor) {
        //Cursorからデータを取得
        String name = getNameByPosition(holder.getAdapterPosition());
        final Gomi data = GomiDaoHelper.getItemByName(name);
        if (data != null) {
            //カードのクリック処理
            holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (mListener != null) {
                        mListener.onEditItem(view, data);
                    }
                    return false;
                }
            });

            //ポップアップボタンのクリック処理
            holder.popupButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    PopupMenu popup = new PopupMenu(mContext, view);
                    popup.getMenuInflater().inflate(R.menu.card_menu_popup, popup.getMenu());
                    popup.show();
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.card_edit:
                                    //編集をコールバック通知
                                    if (mListener != null) {
                                        mListener.onEditItem(holder.cardView, data);
                                    }
                                    break;
                                case R.id.card_delete:
                                    remove(data, holder.getAdapterPosition());
                                    break;
                                case R.id.card_move_up:
                                    //上へ移動
                                    moveup(holder.getAdapterPosition());
                                    break;
                                case R.id.card_move_down:
                                    //下へ移動
                                    movedown(holder.getAdapterPosition());
                                    break;
                            }
                            return true;
                        }
                    });
                }
            });

            //ゴミ名をセット
            String garbageName = data.getName();
            holder.garbageName.setText(garbageName);

            //カラーをセット
            int color = data.getColor();
            holder.garbageName.setTextColor(getColorByColorLabelId(color));
            holder.pattern.setBackgroundResource(getWeekBackgroundDrawablefromColor(color));
            holder.weekOfDay1.setBackgroundResource(getWeekOfDayBackgroundDrawablefromColor(color));
            holder.weekOfDay2.setBackgroundResource(getWeekOfDayBackgroundDrawablefromColor(color));
            holder.weekOfDay3.setBackgroundResource(getWeekOfDayBackgroundDrawablefromColor(color));

            //週表示をセット
            int collectionType = data.getCollectionType();
            if (collectionType == GomiData.CollectionType.EVERY_WEEK) {
                holder.pattern.setText(mContext.getString(R.string.card_week_every));
            } else if (collectionType == GomiData.CollectionType.SELECT_WEEK) {
                String week = data.getCollectionWeek();
                StringBuilder sb = new StringBuilder();
                if (!TextUtils.isEmpty(week)) {
                    StringTokenizer st = new StringTokenizer(week, ",");
                    while (st.hasMoreTokens()) {
                        sb.append(mContext.getString(R.string.card_week_select, st.nextToken()))
                                .append("・");
                    }
                    String weekLabel = sb.toString();
                    if (!TextUtils.isEmpty(weekLabel)) {
                        holder.pattern.setText(weekLabel.substring(0, weekLabel.length() - 1));
                    } else {
                        holder.pattern.setVisibility(View.GONE);
                    }
                }
            } else if (collectionType == GomiData.CollectionType.SELECT_DATE) {
                holder.pattern.setVisibility(View.GONE);
            }

            //曜日表示をセット
            String weekOfDay = data.getCollectionWeekOfDay();
            StringBuilder sb = new StringBuilder();
            String[] days = new String[7];
            int i = 0;
            if (!TextUtils.isEmpty(weekOfDay)) {
                StringTokenizer st = new StringTokenizer(weekOfDay, ",");
                while (st.hasMoreTokens()) {
                    days[i] = st.nextToken();
                    i++;
                }
            }
            if (!TextUtils.isEmpty(days[0])) {
                int day = Integer.parseInt(days[0]);
                holder.weekOfDay1.setText(getWeekOfDayLabelfromIndex(day));
            } else {
                holder.weekOfDay1.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(days[1])) {
                int day = Integer.parseInt(days[1]);
                holder.weekOfDay2.setText(getWeekOfDayLabelfromIndex(day));
            } else {
                holder.weekOfDay2.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(days[2])) {
                int day = Integer.parseInt(days[2]);
                holder.weekOfDay3.setText(getWeekOfDayLabelfromIndex(day));
            } else {
                holder.weekOfDay3.setVisibility(View.GONE);
            }
        }
    }

    public void remove(Gomi data, int position) {
        GomiDaoHelper.remove(data);
        notifyItemRemoved(position);
    }

    public void moveup(int position) {
        if (position > 0) {
            notifyItemMoved(position, position - 1);
        }
    }

    public void movedown(int position) {
        if (position + 1 < getItemCount()) {
            notifyItemMoved(position, position + 1);
        }
    }

    @Override
    public int getItemCount() {
        if (mCursor == null) {
            return 0;
        }
        return mCursor.getCount();
    }

    /**
     * カラーIndexからカラーコードを返却.
     */
    private int getColorByColorLabelId(int color) {
        int colorRes = mContext.getResources().getColor(R.color.md_blue_grey_300);
        switch (color) {
            case GomiData.Color.BLUE_GREY:
                colorRes = mContext.getResources().getColor(R.color.md_blue_grey_300);
                break;
            case GomiData.Color.RED:
                colorRes = mContext.getResources().getColor(R.color.md_red_300);
                break;
            case GomiData.Color.DEEP_ORANGE:
                colorRes = mContext.getResources().getColor(R.color.md_deep_orange_300);
                break;
            case GomiData.Color.INDIGO:
                colorRes = mContext.getResources().getColor(R.color.md_indigo_300);
                break;
            case GomiData.Color.DEEP_PURPLE:
                colorRes = mContext.getResources().getColor(R.color.md_deep_purple_300);
                break;
            case GomiData.Color.GREEN:
                colorRes = mContext.getResources().getColor(R.color.md_green_300);
                break;
            case GomiData.Color.TEAL:
                colorRes = mContext.getResources().getColor(R.color.md_teal_300);
                break;
        }
        return colorRes;
    }

    /**
     * カラーIndexから週の背景を返却.
     */
    private int getWeekBackgroundDrawablefromColor(int color) {
        int bgRes = R.drawable.bg_pattern_blue_grey;
        switch (color) {
            case GomiData.Color.BLUE_GREY:
                bgRes = R.drawable.bg_pattern_blue_grey;
                break;
            case GomiData.Color.RED:
                bgRes = R.drawable.bg_pattern_red;
                break;
            case GomiData.Color.DEEP_ORANGE:
                bgRes = R.drawable.bg_pattern_deep_orange;
                break;
            case GomiData.Color.INDIGO:
                bgRes = R.drawable.bg_pattern_indigo;
                break;
            case GomiData.Color.DEEP_PURPLE:
                bgRes = R.drawable.bg_pattern_deep_purple;
                break;
            case GomiData.Color.GREEN:
                bgRes = R.drawable.bg_pattern_green;
                break;
            case GomiData.Color.TEAL:
                bgRes = R.drawable.bg_pattern_teal;
                break;
        }
        return bgRes;
    }

    /**
     * カラーIndexから曜日の背景を返却.
     */
    private int getWeekOfDayBackgroundDrawablefromColor(int color) {
        int bgRes = R.drawable.selector_color_blue_grey;
        switch (color) {
            case GomiData.Color.BLUE_GREY:
                bgRes = R.drawable.selector_color_blue_grey;
                break;
            case GomiData.Color.RED:
                bgRes = R.drawable.selector_color_red;
                break;
            case GomiData.Color.DEEP_ORANGE:
                bgRes = R.drawable.selector_color_deep_orange;
                break;
            case GomiData.Color.INDIGO:
                bgRes = R.drawable.selector_color_indigo;
                break;
            case GomiData.Color.DEEP_PURPLE:
                bgRes = R.drawable.selector_color_deep_purple;
                break;
            case GomiData.Color.GREEN:
                bgRes = R.drawable.selector_color_green;
                break;
            case GomiData.Color.TEAL:
                bgRes = R.drawable.selector_color_teal;
                break;
        }
        return bgRes;
    }

    private String getWeekOfDayLabelfromIndex(int index) {
        String label = null;
        switch (index) {
            case 1:
                label = mContext.getString(R.string.card_dayofweek_mon);
                break;
            case 2:
                label = mContext.getString(R.string.card_dayofweek_tue);
                break;
            case 3:
                label = mContext.getString(R.string.card_dayofweek_wed);
                break;
            case 4:
                label = mContext.getString(R.string.card_dayofweek_thu);
                break;
            case 5:
                label = mContext.getString(R.string.card_dayofweek_fri);
                break;
            case 6:
                label = mContext.getString(R.string.card_dayofweek_sat);
                break;
            case 7:
                label = mContext.getString(R.string.card_dayofweek_sun);
                break;
        }
        return label;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.card_view)
        CardView cardView;

        @Bind(R.id.popup_button)
        ImageButton popupButton;

        @Bind(R.id.garbage_name)
        NotoJpTextView garbageName;

        @Bind(R.id.date)
        NotoJpTextView date;

        @Bind(R.id.pattern)
        NotoJpTextView pattern;

        @Bind(R.id.week_of_day_1)
        NotoJpTextView weekOfDay1;

        @Bind(R.id.week_of_day_2)
        NotoJpTextView weekOfDay2;

        @Bind(R.id.week_of_day_3)
        NotoJpTextView weekOfDay3;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            garbageName.setTypeface(Typeface.DEFAULT, Typeface.ITALIC);
        }
    }

    /**
     * 指定したポジションのIDを取得.
     *
     * @param position ポジション
     */
    public String getNameByPosition(int position) {
        String name = null;
        if (mCursor.moveToPosition(position)) {
            name = mCursor.getString(mCursor.getColumnIndex(GomiDao.Properties.Name.columnName));
        }
        return name;
    }

    public interface OnActionItem {

        public void onEditItem(View view, Gomi data);
    }

    public void setOnActionItemListener(OnActionItem listener) {
        mListener = listener;
    }

}

