package jp.co.android.miyavi.gomisute.rule;

import com.rey.material.widget.ProgressView;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.math.BigDecimal;

import butterknife.Bind;
import jp.co.android.miyavi.gomisute.BaseActivity;
import jp.co.android.miyavi.gomisute.util.WebViewUtil;
import jp.co.miyavi.android.gomisute.R;

public class RulePageActivity extends BaseActivity {

    /** 定数 */
    private static final String MAIL_LINK = "mailto:";

    public static final String INTENT_KEY_URL = "intent-key-url";

    /** フィールド */
    @Bind(R.id.webView)
    WebView mWebView;

    @Bind(R.id.loadingBar)
    ProgressView mPbLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //ToolBarの初期化
        setScreenTitle(R.string.action_rule);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        WebViewUtil.setup(mWebView);
        mWebView.setWebViewClient(new MyWebViewClient());
        mWebView.setWebChromeClient(new MyWebChromeClient());

        //URLをロード
        String url = null;
        Intent intent = getIntent();
        if (intent != null) {
            url = intent.getStringExtra(INTENT_KEY_URL);
        }
        if (!TextUtils.isEmpty(url)) {
            mWebView.loadUrl(url);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_rule;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mWebView.canGoBack()) {
                    mWebView.goBack();
                } else {
                    finish();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * カスタムWebViewClient.
     */
    public class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (mPbLoading != null) {
                mPbLoading.stop();
            }
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if (mPbLoading != null) {
                mPbLoading.start();
            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Intent intent = null;
            if (url.substring(0, MAIL_LINK.length()).equals(MAIL_LINK)) {
                intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(url));
            } else if (url.contains("twitter") || url.contains("facebook")) {
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            }
            if (intent != null) {
                try {
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(RulePageActivity.this, getString(R.string.error_app_none),
                            Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                mWebView.reload();
                return true;
            }
            return super.shouldOverrideUrlLoading(view, url);
        }
    }

    /**
     * カスタムWebChromeClient.
     */
    public class MyWebChromeClient extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            if (mPbLoading != null) {
                //ProgressViewが0～1で進捗表示するため100分の1する
                BigDecimal dec = new BigDecimal(newProgress);
                float progress = dec.movePointLeft(2).floatValue();
                mPbLoading.setProgress(progress);
                //10%超えたらタイトルを変更
                if (newProgress > 0.1) {
                    setScreenTitle(view.getTitle());
                }
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (mWebView.canGoBack()) {
                mWebView.goBack();
                return false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}