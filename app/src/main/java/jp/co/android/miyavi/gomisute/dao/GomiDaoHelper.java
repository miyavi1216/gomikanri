
package jp.co.android.miyavi.gomisute.dao;

import android.text.TextUtils;

import greendao.Gomi;
import greendao.GomiDao;
import jp.co.android.miyavi.gomisute.MyApplication;

/**
 * ゴミデータベースのヘルパークラス.
 */
public class GomiDaoHelper {

    /**
     * ゴミデータを保存.
     */
    public static void save(Gomi data) {
        GomiDao gomiDao = MyApplication.getInstance().getDaoSession().getGomiDao();

        //DBに同じインフォメーションデータがあるか確認
        Gomi entry = getItemByName(data.getName());
        if (entry != null) {
            //DB上に存在したらデータを上書き
            data.setId(entry.getId());
            data.setName(entry.getName());
            data.setDisplayName(entry.getDisplayName());
            data.setColor(entry.getColor());
            data.setCollectionType(entry.getCollectionType());
            data.setCollectionWeek(entry.getCollectionWeek());
            data.setCollectionWeekOfDay(entry.getCollectionWeekOfDay());
            data.setCollectionDate(entry.getCollectionDate());
            data.setRemind(entry.getRemind());
            data.setRemindType(entry.getRemindType());
            data.setRemindHour(entry.getRemindHour());
            data.setRemindMinutes(entry.getRemindMinutes());
            data.setCreatedTime(entry.getCreatedTime());
        }
        gomiDao.insertOrReplace(data);
    }

    /**
     * ゴミデータを削除.
     */
    public static void remove(Gomi data) {
        GomiDao gomiDao = MyApplication.getInstance().getDaoSession().getGomiDao();
        gomiDao.delete(data);
    }

    /**
     * 指定した名前のゴミデータを取得.
     */
    public static Gomi getItemByName(String name) {
        if (TextUtils.isEmpty(name)) {
            return null;
        }
        GomiDao gomiDao = MyApplication.getInstance().getDaoSession().getGomiDao();
        Gomi entry = gomiDao.queryBuilder().where(GomiDao.Properties.Name.eq(name)).unique();
        return entry;
    }
}