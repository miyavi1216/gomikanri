package jp.co.android.miyavi.gomisute.view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class MainPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<ViewPagerContainer> mContainers;

    public MainPagerAdapter(FragmentManager fm, ArrayList<ViewPagerContainer> containers) {
        super(fm);
        mContainers = containers;
    }

    @Override
    public Fragment getItem(int position) {
        return mContainers.get(position).getFragment();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mContainers.get(position).getTitle();
    }

    @Override
    public int getItemPosition(Object item) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return mContainers.size();
    }

}
