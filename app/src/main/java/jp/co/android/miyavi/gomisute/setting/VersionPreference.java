package jp.co.android.miyavi.gomisute.setting;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import jp.co.android.miyavi.gomisute.typeface.Typefaces;
import jp.co.miyavi.android.gomisute.R;

/**
 * Created by m_iwasaki on 14/12/01.
 */
public class VersionPreference extends Preference {

    @Bind(R.id.show_version)
    TextView mTvVersion;

    public VersionPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        setWidgetLayoutResource(R.layout.pref_versionview);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);
        ButterKnife.bind(this, view);

        //バージョン表示
        String version = getVersion();
        Typeface face = Typefaces.getInstance(getContext());
        mTvVersion.setTypeface(face, Typeface.NORMAL);
        mTvVersion.setText(version);
    }

    /**
     * バージョン情報を取得.
     */
    private String getVersion() {
        String version = "version";
        String versionName = "1.0";
        PackageManager packageManager = getContext().getPackageManager();
        try {
            String packageName = getContext().getPackageName();
            PackageInfo packageInfo = packageManager.getPackageInfo(packageName,
                    PackageManager.GET_ACTIVITIES);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version + " " + versionName;
    }
}
