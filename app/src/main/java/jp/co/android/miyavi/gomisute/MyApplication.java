package jp.co.android.miyavi.gomisute;

import com.google.android.gms.analytics.Tracker;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import greendao.DaoMaster;
import greendao.DaoSession;
import jp.co.android.miyavi.gomisute.analytics.GAManager;

/**
 * 本アプリのアプリケーションクラス.
 */
public class MyApplication extends Application {

    private final String TAG = MyApplication.class.getSimpleName();

    private static MyApplication mInstance;

    private GAManager mAnalyticsManager;

    public MyApplication() {
        mInstance = this;
    }

    public DaoSession mDaoSession;

    private DaoMaster mDaoMaster;

    public static MyApplication getInstance() {
        return MyApplication.mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mAnalyticsManager = new GAManager(this);
    }

    public DaoMaster getDaoMaster() {
        if (mDaoMaster == null) {
            DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "gomi.db", null);
            SQLiteDatabase db = helper.getWritableDatabase();
            mDaoMaster = new DaoMaster(db);
        }

        return mDaoMaster;
    }

    /**
     * DBをセットアップ.
     */
    private DaoSession setupDatabase() {
        return getDaoMaster().newSession();
    }

    /**
     * DaoSessionを取得.
     */
    public synchronized DaoSession getDaoSession() {
        if (mDaoSession == null) {
            mDaoSession = setupDatabase();
        }
        return mDaoSession;
    }

    /**
     * GoogleAnalytics Trackerの取得.
     */
    public Tracker getTracker(GAManager.TrackerName trackerId) {
        return mAnalyticsManager.getTracker(trackerId);
    }
}
