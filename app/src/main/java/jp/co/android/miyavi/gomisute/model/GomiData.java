package jp.co.android.miyavi.gomisute.model;

import android.content.Context;
import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;

import greendao.Gomi;
import jp.co.miyavi.android.gomisute.R;

/**
 * Created by m_iwasaki on 15/07/06.
 */
public class GomiData implements Parcelable {

    //Intent keys
    public static interface IntentKey {

        public static final String ID = "intent_key_id";
        public static final String NAME = "intent_key_name";
        public static final String DISPLAY_NAME = "intent_key_display_name";
        public static final String COLOR = "intent_key_color";
        public static final String COLLECTION_TYPE = "intent_key_collection_type";
        public static final String COLLECTION_WEEK = "intent_key_collection_week";
        public static final String COLLECTION_WEEK_OF_DAY = "intent_key_collection_week_of_day";
        public static final String COLLECTION_DATE = "intent_key_collection_date";
        public static final String REMIND = "intent_key_remind";
        public static final String REMIND_TYPE = "intent_key_remind_type";
        public static final String REMIND_HOUR = "intent_key_remind_hour";
        public static final String REMIND_MINUTES = "intent_key_remind_minutes";
        public static final String CREATED_TIME = "created_time";
    }

    public static interface Color {

        public static final int BLUE_GREY = 1;
        public static final int RED = 2;
        public static final int DEEP_ORANGE = 3;
        public static final int INDIGO = 4;
        public static final int DEEP_PURPLE = 5;
        public static final int GREEN = 6;
        public static final int TEAL = 7;
    }

    //収集日タイプ
    public static interface CollectionType {

        public static final int EVERY_WEEK = 0;
        public static final int SELECT_WEEK = 1;
        public static final int SELECT_DATE = 2;
    }

    //通知設定
    public static interface Remind {

        public static final int OFF = 0;
        public static final int ON = 1;
    }

    //通知タイプ
    public static interface RemindType {

        public static final int BEFOREDAY = 0;
        public static final int TODAY = 1;
    }

    public static final Creator<GomiData> CREATOR = new Creator<GomiData>() {
        @Override
        public GomiData createFromParcel(Parcel in) {
            return new GomiData(in);
        }

        @Override
        public GomiData[] newArray(int size) {
            return new GomiData[size];
        }
    };

    private long id;

    private String name;

    private String displayName;

    private int color;

    private int collectionType;

    private String collectionWeek;

    private String collectionWeekOfDay;

    private long collectionDate;

    private int remind;

    private int remindType;

    private int remindHour;

    private int remindMinutes;

    private long createdTime;

    public GomiData() {
        color = Color.BLUE_GREY;
        collectionType = CollectionType.EVERY_WEEK;
        remind = Remind.OFF;
        remindType = RemindType.BEFOREDAY;
    }

    public GomiData(Gomi data) {
        id = data.getId();
        name = data.getName();
        displayName = data.getDisplayName();
        color = data.getColor();
        collectionType = data.getCollectionType();
        collectionWeek = data.getCollectionWeek();
        collectionWeekOfDay = data.getCollectionWeekOfDay();
        collectionDate = data.getCollectionDate();
        remind = data.getRemind();
        remindType = data.getRemindType();
        remindHour = data.getRemindHour();
        remindMinutes = data.getRemindMinutes();
        createdTime = data.getCreatedTime();
    }

    public GomiData(Intent intent) {
        id = intent.getLongExtra(IntentKey.ID, 0);
        name = intent.getStringExtra(IntentKey.NAME);
        displayName = intent.getStringExtra(IntentKey.DISPLAY_NAME);
        color = intent.getIntExtra(IntentKey.COLOR, Color.BLUE_GREY);
        collectionType = intent.getIntExtra(IntentKey.COLLECTION_TYPE, CollectionType.EVERY_WEEK);
        collectionWeek = intent.getStringExtra(IntentKey.COLLECTION_WEEK);
        collectionWeekOfDay = intent.getStringExtra(IntentKey.COLLECTION_WEEK_OF_DAY);
        collectionDate = intent.getLongExtra(IntentKey.COLLECTION_DATE, 0);
        remind = intent.getIntExtra(IntentKey.REMIND, Remind.OFF);
        remindType = intent.getIntExtra(IntentKey.REMIND_TYPE, RemindType.BEFOREDAY);
        remindHour = intent.getIntExtra(IntentKey.REMIND_HOUR, 0);
        remindMinutes = intent.getIntExtra(IntentKey.REMIND_MINUTES, 0);
        createdTime = intent.getLongExtra(IntentKey.CREATED_TIME, 0);
    }

    public GomiData(Parcel in) {
        id = in.readLong();
        name = in.readString();
        displayName = in.readString();
        color = in.readInt();
        collectionType = in.readInt();
        collectionWeek = in.readString();
        collectionWeekOfDay = in.readString();
        collectionDate = in.readLong();
        remind = in.readInt();
        remindType = in.readInt();
        remindHour = in.readInt();
        remindMinutes = in.readInt();
        createdTime = in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeString(displayName);
        parcel.writeInt(color);
        parcel.writeInt(collectionType);
        parcel.writeString(collectionWeek);
        parcel.writeString(collectionWeekOfDay);
        parcel.writeLong(collectionDate);
        parcel.writeInt(remind);
        parcel.writeInt(remindType);
        parcel.writeInt(remindHour);
        parcel.writeInt(remindMinutes);
        parcel.writeLong(createdTime);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getCollectionType() {
        return collectionType;
    }

    public void setCollectionType(int collectionType) {
        this.collectionType = collectionType;
    }

    public String getCollectionWeek() {
        return collectionWeek;
    }

    public void setCollectionWeek(String collectionWeek) {
        this.collectionWeek = collectionWeek;
    }

    public String getCollectionWeekOfDay() {
        return collectionWeekOfDay;
    }

    public void setCollectionWeekOfDay(String collectionWeekOfDay) {
        this.collectionWeekOfDay = collectionWeekOfDay;
    }

    public long getCollectionDate() {
        return collectionDate;
    }

    public void setCollectionDate(long collectionDate) {
        this.collectionDate = collectionDate;
    }

    public int getRemind() {
        return remind;
    }

    public void setRemind(int remind) {
        this.remind = remind;
    }

    public int getRemindType() {
        return remindType;
    }

    public void setRemindType(int remindType) {
        this.remindType = remindType;
    }

    public int getRemindHour() {
        return remindHour;
    }

    public void setRemindHour(int remindHour) {
        this.remindHour = remindHour;
    }

    public int getRemindMinutes() {
        return remindMinutes;
    }

    public void setRemindMinutes(int remindMinutes) {
        this.remindMinutes = remindMinutes;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }

    public static Gomi parseGomiData(GomiData data) {
        Gomi value = null;
        if (data != null) {
            value = new Gomi();
            value.setName(data.getName());
            value.setDisplayName(data.getDisplayName());
            value.setColor(data.getColor());
            value.setCollectionType(data.getCollectionType());
            value.setCollectionWeek(data.getCollectionWeek());
            value.setCollectionWeekOfDay(data.getCollectionWeekOfDay());
            value.setCollectionDate(data.getCollectionDate());
            value.setRemind(data.getRemind());
            value.setRemindType(data.getRemindType());
            value.setRemindHour(data.getRemindHour());
            value.setRemindMinutes(data.getRemindMinutes());
            value.setCreatedTime(data.getCreatedTime());
        }
        return value;
    }
}