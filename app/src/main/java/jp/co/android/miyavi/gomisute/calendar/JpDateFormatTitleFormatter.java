package jp.co.android.miyavi.gomisute.calendar;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.format.TitleFormatter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Format using a {@linkplain DateFormat} instance.
 */
public class JpDateFormatTitleFormatter implements TitleFormatter {

    private final DateFormat dateFormat;

    /**
     * Uses "MMMM yyyy" for formatting
     */
    public JpDateFormatTitleFormatter() {
        this.dateFormat = new SimpleDateFormat(
                "yyyy年 MMMM", Locale.getDefault()
        );
    }

    /**
     * @param format the format to use
     */
    public JpDateFormatTitleFormatter(DateFormat format) {
        this.dateFormat = format;
    }

    @Override
    public CharSequence format(CalendarDay day) {
        return dateFormat.format(day.getDate());
    }
}
