package jp.co.android.miyavi.gomisute.setting;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import jp.co.android.miyavi.gomisute.BaseActivity;
import jp.co.miyavi.android.gomisute.R;

/**
 * Created by m_iwasaki on 14/12/01.
 */
public class SettingActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //ToolBarの初期化
        setScreenTitle(R.string.action_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //設定画面をロード
        getFragmentManager().beginTransaction().
                replace(R.id.contentFrame, new PrefsFragment()).commit();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_setting;
    }

    public static class PrefsFragment extends PreferenceFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
        }
    }
}