package jp.co.android.miyavi.gomisute.util;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.transition.Slide;
import android.view.Gravity;
import android.view.animation.BounceInterpolator;

public class FragmentUtil {

    /** Fragment no stack */
    public static final int NO_STACK = 0;

    /** Fragment back stack */
    public static final int BACK_STACK = 1;

    /** Animation Type fade */
    public static final int ANIM_FADE = 0;

    /** Animation Type bounce */
    public static final int ANIM_BOUNCE = 1;

    /** Activity */
    private ActionBarActivity mActivity;

    /** Layout ID */
    private int mLayoutId = -1;

    /** FragmentManager */
    private FragmentManager mFragmentManager;

    /**
     * Constructor.
     */
    public FragmentUtil(ActionBarActivity activity, int layoutId) {
        mActivity = activity;
        mFragmentManager = mActivity.getFragmentManager();
        mLayoutId = layoutId;
    }


    /**
     * Replace the Fragment.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void replaceFragment(Fragment fragment, String tag, int animatinoType, int isStack) {
        if (mActivity == null || mFragmentManager == null || mLayoutId == -1) {
            return;
        }
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        if (Funcs.isLollipop() && animatinoType != ANIM_FADE) {
            Slide transition = new Slide();
            transition.setSlideEdge(Gravity.TOP);
            transition.setDuration(800);
            transition.setInterpolator(new BounceInterpolator());
            fragment.setEnterTransition(transition);
            fragment.setSharedElementEnterTransition(transition);
        } else {
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        }
        Fragment currentFragment = mFragmentManager.findFragmentById(mLayoutId);
        if (currentFragment != null) {
            ft.remove(currentFragment);
        }
        ft.replace(mLayoutId, fragment, tag);
        if (isStack == BACK_STACK) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }

    /**
     * Replace the Fragment with AllowingStateLoss.
     */
    public void replaceFragmentAllowingStateLoss(Fragment fragment, String tag, int isStack) {
        if (mActivity == null || mFragmentManager == null || mLayoutId == -1) {
            return;
        }
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        Fragment currentFragment = mFragmentManager.findFragmentById(mLayoutId);
        if (currentFragment != null) {
            ft.remove(currentFragment);
        }
        ft.replace(mLayoutId, fragment, tag);
        if (isStack == BACK_STACK) {
            ft.addToBackStack(tag);
        }
        ft.commitAllowingStateLoss();
    }

    /**
     * Add the Fragment.
     */
    public void addFragment(Fragment fragment, String tag, int isStack) {
        if (mActivity == null || mFragmentManager == null || mLayoutId == -1) {
            return;
        }

        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.add(mLayoutId, fragment, tag);
        if (isStack == BACK_STACK) {
            ft.addToBackStack(tag);
        }
        ft.commit();
    }

    /**
     * Add the Fragment with AllowingStateLoss.
     */
    public void addFragmentAllowingStateLoss(Fragment fragment, String tag, int isStack) {
        if (mActivity == null || mFragmentManager == null || mLayoutId == -1) {
            return;
        }
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.add(mLayoutId, fragment, tag);
        if (isStack == BACK_STACK) {
            ft.addToBackStack(tag);
        }
        ft.commitAllowingStateLoss();
    }

    /**
     * Remove the Fragment.
     */
    public void removeFragment(Fragment fragment) {
        if (mActivity == null || mFragmentManager == null || mLayoutId == -1) {
            return;
        }
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.remove(fragment);
        ft.commit();
    }

    /**
     * Have a stack in FragmentManager.
     */
    public boolean isStack() {
        boolean isStack = false;
        if (mActivity == null || mFragmentManager == null || mLayoutId == -1) {
            return false;
        }

        int stack = mFragmentManager.getBackStackEntryCount();
        if (stack == 0) {
            isStack = false;
        } else {
            isStack = true;
        }
        return isStack;
    }

    /**
     * Fragment show in activity.
     */
    public boolean isShown(String tag) {
        boolean isShow = false;
        if (mActivity == null || mFragmentManager == null || mLayoutId == -1) {
            return false;
        }

        Fragment basicFragment = getFragmentByTag(tag);
        Fragment currentFragment = getCurrentFragment();
        if (basicFragment.equals(currentFragment)) {
            isShow = true;
        }
        return isShow;
    }

    /**
     * Fragment stack index of top.
     */
    public boolean isTop(Fragment fragment) {
        boolean isTop = false;
        if (mActivity == null || mFragmentManager == null || mLayoutId == -1) {
            return false;
        }

        Fragment current = mFragmentManager.findFragmentById(mLayoutId);
        if (fragment.equals(current)) {
            isTop = true;
        } else {
            isTop = false;
        }
        return isTop;
    }

    /**
     * Get FragmentManager.
     */
    public FragmentManager getFragmentManager() {
        return mFragmentManager;
    }

    /**
     * Get Current Fragment by tag.
     */
    public Fragment getFragmentByTag(String tag) {
        if (mActivity == null || mFragmentManager == null || mLayoutId == -1) {
            return null;
        }
        return mFragmentManager.findFragmentByTag(tag);
    }

    /**
     * Get Current Fragment.
     */
    public Fragment getCurrentFragment() {
        if (mActivity == null || mFragmentManager == null || mLayoutId == -1) {
            return null;
        }
        return mFragmentManager.findFragmentById(mLayoutId);
    }

    /**
     * Set layout Id
     */
    public void setLayoutId(int mLayoutId) {
        this.mLayoutId = mLayoutId;
    }
}
