
package jp.co.android.miyavi.gomisute.view;

import android.support.v4.app.Fragment;

/**
 * ViewPagerの子Fragmentの情報クラス
 */
public class ViewPagerContainer {
    Fragment fragment;
    String title;

    public ViewPagerContainer() {
        this.fragment = null;
        this.title = "";
    }

    public ViewPagerContainer(Fragment fragment, String title) {
        this.fragment = fragment;
        this.title = title;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
