package jp.co.android.miyavi.gomisute.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.SystemClock;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;

import java.io.File;
import java.lang.reflect.Field;

public class WebViewUtil {

    @SuppressLint("SetJavaScriptEnabled")
    public static void setup(WebView webView) {
        webView.setScrollBarStyle(WebView.SCROLLBARS_INSIDE_OVERLAY);
        webView.setFocusableInTouchMode(true);// タッチした時にfocusをあてる
        webView.setNetworkAvailable(true); // window.navigator.isOnline を有効にする
        // webView.setLongClickable(true);

        // WebSettings
        WebSettings ws = webView.getSettings();
        ws.setJavaScriptEnabled(true);

        String internalDatabasePath = getInternalDatabasePath(webView.getContext());

        // HTML5 API: オフラインモード対応 (.appcacheを使用するか)
        // ws.setAppCacheMaxSize(1024 * 1024 * 8);
        ws.setAppCachePath(internalDatabasePath);
        ws.setAppCacheEnabled(true);

        // HTML5 API: Web database API に対応するかどうか
        // (おそらくWebSQLDatabaseやIndexedDB)の対応
        ws.setDatabasePath(internalDatabasePath);
        ws.setDatabaseEnabled(true);

        // WebStorage(localStorage)を利用するかどうか
        ws.setDomStorageEnabled(true);

        // Geolocation対応後に有効化 (位置情報取得の許可ウィンドウの生成等)
        ws.setGeolocationDatabasePath(internalDatabasePath);
        ws.setGeolocationEnabled(true);

        // 詳細が不明なメソッドだが、
        // これがtrueになっている場合、ICSにてクリックできないリンクが出てくる。
        ws.setLightTouchEnabled(false);

        // HTMLをデコードする際のデフォルトのエンコーディング名を指定する
        // ws.setDefaultTextEncodingName(null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            ws.setPluginState(PluginState.ON_DEMAND);
        } else {
            ws.setPluginState(PluginState.ON);
        }

        ws.setLoadsImagesAutomatically(true);

        ws.setSavePassword(true);
        ws.setSaveFormData(true);

        // PCサイトでも拡大縮小表示ができるように
        ws.setUseWideViewPort(true);

        // PC画面を全体で見れるようにする設定
        ws.setLoadWithOverviewMode(true);

        // 描画の優先順位を設定する
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            // JB_MR2で非推奨。
            ws.setRenderPriority(RenderPriority.HIGH);
        }

        // 拡大縮小を有効にする。
        //ws.setDefaultZoom(ZoomDensity.MEDIUM);
        ws.setBuiltInZoomControls(true);
        ws.setSupportZoom(true);

        try {
            // マルチタッチを有効にしたまま、zoom controlを消す
            Field nameField = ws.getClass().getDeclaredField(
                    "mBuiltInZoomControls");
            nameField.setAccessible(true);
            nameField.set(ws, false);
        } catch (Exception e) {
            ws.setDisplayZoomControls(false);
        }
    }

    public static final String getInternalDatabasePath(Context context) {
        File databasesDir = context.getDir("databases", Context.MODE_PRIVATE);
        if (!databasesDir.exists()) {
            boolean isSuccess = databasesDir.mkdirs();
            if (!isSuccess) {
                // 本体の容量が一杯だとか。
                // この場合動作させることが出来ない。
                throw new RuntimeException("failure : create database.");
            }
        }

        return databasesDir.getAbsolutePath();
    }

    /**
     * アプリ上のWebViewのセッションを削除する.
     */
    public static void removeSessionCookie(Context context) {
        CookieSyncManager.createInstance(context);
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeSessionCookie();
        CookieSyncManager.getInstance().sync();
        SystemClock.sleep(1000);
    }

    /**
     * アプリ上のWebViewのキャッシュを削除する.
     */
    public static void clearCache(WebView wb) {
        wb.clearHistory();
        wb.clearFormData();
        wb.clearCache(true);
    }
}
