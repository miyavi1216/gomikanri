package jp.co.android.miyavi.gomisute.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import jp.co.android.miyavi.gomisute.typeface.NotoJpTypefaces;

/**
 * Created by m_iwasaki on 15/06/30.
 */
public class NotoJpTextView extends TextView {

    public NotoJpTextView(Context context) {
        super(context);
        setupTypeface(context);
    }

    public NotoJpTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupTypeface(context);
    }

    public NotoJpTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupTypeface(context);
    }

    public void setupTypeface(Context context) {
        Typeface face = NotoJpTypefaces.getInstance(context);
        setTypeface(face, Typeface.NORMAL);
        setIncludeFontPadding(false);
    }

    @Override
    public void setTypeface(Typeface tf, int style) {
        Typeface face = NotoJpTypefaces.getInstance(getContext());
        super.setTypeface(face, style);
    }
}
