package jp.co.android.miyavi.gomisute;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class MyDaoGenerator {

    private static final int DATABASE_VERSION = 1;

    private static final String DEFAULT_PACKAGE = "greendao";


    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(DATABASE_VERSION, DEFAULT_PACKAGE);
        createGomiTable(schema);
        new DaoGenerator().generateAll(schema, args[0]);
    }

    /**
     * ゴミデータテーブル.
     */
    private static Entity createGomiTable(Schema schema) {
        Entity entity = schema.addEntity("Gomi");
        entity.addIdProperty();
        entity.addStringProperty("Name");
        entity.addStringProperty("DisplayName");
        entity.addIntProperty("Color");
        entity.addIntProperty("CollectionType");
        entity.addStringProperty("CollectionWeek");
        entity.addStringProperty("CollectionWeekOfDay");
        entity.addLongProperty("CollectionDate");
        entity.addIntProperty("Remind");
        entity.addIntProperty("RemindType");
        entity.addIntProperty("RemindHour");
        entity.addIntProperty("RemindMinutes");
        entity.addLongProperty("CreatedTime");
        return entity;
    }
}
